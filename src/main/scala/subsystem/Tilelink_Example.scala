package freechips.rocketchip.subsystem
import Chisel._
import freechips.rocketchip.config.{Field, Parameters}
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.tilelink._

class TLaddr extends Bundle {
  val addr        = UInt(39.W) // 39 phys addr bits
}
class TLdata extends Bundle {
  val data        = UInt(64.W) // 64b TL interface  
}
class tile_blackbox extends BlackBox {
 val io = IO(new Bundle { // using scala's anonymous class syntax
	val tile_a_chan = Decoupled(new TLaddr)
    val tile_d_chan = Decoupled(new TLdata)      
})}

//You can expose a lot of tilelink ports like (peripheral bus,system bus, various crossbars that are instantiated inside BaseSubsystem) by extending basesubsystem of rocket-chip, then using the same names to wire your own chisel modules to them.
trait HasExampleTileOuter{ this: BaseSubsystem =>
  private val portName = "example-tilelink"
  val exampleInstance = LazyModule(new ExampleTileModule)

  val managernode = locateTLBusWrapper(FBUS)
  managernode.fromPort(Some(portName))() :=* exampleInstance.dmanode0 

  }

trait HasExampleTileInner extends LazyModuleImp {
  val outer: HasExampleTileOuter
}

class ExampleTileModule(implicit p: Parameters) extends LazyModule {

	val device = new SimpleDevice("DTname", Seq("IntenSivate"))

	val dmanode0 = TLClientNode(Seq(TLClientPortParameters(
		Seq(TLClientParameters(
			name = "YourTilelinkModulename",
			sourceId = IdRange(0, 1))))))
 
	lazy val module = new ExampleTileModuleImp(this)

}

// Actual Device RTL
class ExampleTileModuleImp (outer: ExampleTileModule) extends LazyModuleImp(outer) {
	val (tl0, edge0) = outer.dmanode0.out(0)
	val addrBits0 = edge0.bundle.addressBits
	val beatBytes = (edge0.bundle.dataBits / 8)

	val blackbox_inst = Module(new tile_blackbox)
	tl0.a <> blackbox_inst.io.tile_a_chan
	tl0.d <> blackbox_inst.io.tile_d_chan

}
